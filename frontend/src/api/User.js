import axios from 'axios';

const api = axios.create({
    baseURL: "https://localhost:7235/api"
});

export async function saveUser(userData){
    const response = await api.post(`/user/save`, userData);
    return response;
}