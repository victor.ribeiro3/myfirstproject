import axios from 'axios';

const api = axios.create({
    baseURL: "https://plataforma.bigdatacorp.com.br/",
    headers:{
        'TokenID': process.env.REACT_APP_PLATFORM_TOKEN_ID,
        'AccessToken': process.env.REACT_APP_PLATFORM_ACCESS_TOKEN
    }
});

export async function getBasicData(userCPF){
    const response = await api.post(`/pessoas`, 
    {
        'q':`DOC{${userCPF}}`,
        'Datasets': 'basic_data'
    });
    return response;
}
