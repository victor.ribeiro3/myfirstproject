import axios from 'axios';

const api = axios.create({
    baseURL: "https://plugin.bigdatacorp.com.br/",
    headers:{
        withCredentials: false,
        'Content-Type': 'text/plain',
        'TokenID': process.env.REACT_APP_PLATFORM_TOKEN_ID,
        'AccessToken': process.env.REACT_APP_PLATFORM_ACCESS_TOKEN
    }
});

export async function validateEmailAddress(userEmail){
    try{
        const response = await api.post(`validacoes`, 
        {
            'q':`email{${userEmail}}`,
            'Services': 'ondemand_email'
        });
        return response;
    }catch(e){
        console.error(e);
    }
    
}
