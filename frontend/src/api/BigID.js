import axios from 'axios';

const api = axios.create({
    baseURL: "https://app.bigdatacorp.com.br/bigid/",
    headers:{
        'TokenID': process.env.REACT_APP_BIGID_TOKEN_ID,
        'AccessToken': process.env.REACT_APP_BIGID_ACCESS_TOKEN
    }
});

export async function generateQuestionnaire(userCPF){
    const response = await api.post(`kba/perguntas`, 
    {
        'Parameters':[`CPF=${userCPF}`]
    });
    return response;
}

export async function sendQuestionnaireAnswers(ticketId, answers){
    const response = await api.post(`kba/respostas`, 
    {
        'TicketId': ticketId,
        'Parameters': answers
    });
    return response;
}
