
import '../index.css';
import { useState } from "react";
import InputMask from 'react-input-mask';
import { getBasicData } from '../api/Platform';
import { validateEmailAddress } from '../api/Plugin';
import { generateQuestionnaire } from '../api/BigID';
import CircularProgress from '@mui/material/CircularProgress';
import Button from '@mui/material/Button';
import { ThemeProvider } from '@mui/material/styles';
import customTheme from './Theme';
import Notification from './Notification';

function SaveForm({setOpenQuestionnaire, setQuestionnaireData, formData, setFormData, showNotification}) {
  
  const [formError, setFormError] = useState({cpf: "", name: "", email: ""});
  const [cpfTimer, setCpfTimer] = useState(null);
  const [emailTimer, setEmailTimer] = useState(null);
  const [cpfStatus, setCpfStatus] = useState('IRREGULAR');
  const [emailStatus, setEmailStatus] = useState('INVALIDO');
  const [loadingCpf, setLoadingCpf] = useState(false);
  const [loadingEmail, setLoadingEmail] = useState(false);
  const [loadingSave, setLoadingSave] = useState(false);

  const onlyNumbers = (s) => {
    return s.replace(/[^0-9]/g, '');
  };

  const fillFormDataUsingAPI = async (cpf) => {
    let payload;
    setLoadingCpf(true);
    await getBasicData(cpf).then(response => payload = response.data);
    const basic_data = payload.Result[0].BasicData;
    if(basic_data.TaxIdStatus != "REGULAR"){
      showNotification(`CPF irregular: ${basic_data.TaxIdStatus}`, 'error');
      setFormError((prevFormError) => ({ ...prevFormError, ['cpf']: 'CPF irregular.' }));
      setCpfStatus('IRREGULAR');
    }
    else{
      setCpfStatus('REGULAR');
      setFormData((prevFormData) => ({ ...prevFormData, ['name']: basic_data.Name }));
      setFormError((prevFormError) => ({ ...prevFormError, ['name']: '' }));
      // console.log(basic_data.ChineseSign);
    }
    setLoadingCpf(false);
  }

  const checkEmailValidityUsingAPI = async (emailAddr) => {
    let payload;
    setLoadingEmail(true);
    await validateEmailAddress(emailAddr).then(response => {
      try{
        payload = response.data;
        const emailStatus = payload.Result[0].EmailValidations[0].Status;
        if (emailStatus === "VALID")
          setEmailStatus("VALIDO")
        else{
          showNotification("E-mail inválido.", "error");
          setFormError((prevFormError) => ({ ...prevFormError, ['email']: 'E-mail inválido.' }));
          setEmailStatus("INVALIDO");
        }
      }catch(e){
        showNotification("Erro ao acessar a API de validação de e-mail.", "error");
      }finally{
        setLoadingEmail(false);
      }});
  }

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevFormData) => ({ ...prevFormData, [name]: value }));
    setFormError((prevFormError) => ({ ...prevFormError, [name]: '' }));
  };

  const handleCPFChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevFormData) => ({ ...prevFormData, [name]: value }));
    setCpfStatus("IRREGULAR");
    if(e.type == 'change'){
      setFormError((prevFormError) => ({ ...prevFormError, [name]: '' }));
      clearTimeout(cpfTimer);

      const newTimer = setTimeout(() => {
        const cpf = onlyNumbers(value);
        if (cpf.length == 11){
          if(!isCpfValid(cpf)){
            setFormError((prevFormError) => ({ ...prevFormError, ['cpf']: 'CPF inválido.' }));
          }else{
            fillFormDataUsingAPI(cpf);
          }
        }
      }, 500);

      setCpfTimer(newTimer);
    }
  }

  const handleEmailChange = (e) => { 

    const { name, value } = e.target;
    setFormData((prevFormData) => ({ ...prevFormData, [name]: value }));
    setFormError((prevFormError) => ({ ...prevFormError, [name]: '' }));
    setEmailStatus("INVALIDO");
    clearTimeout(emailTimer);
      const newTimer = setTimeout(() => {
        const email = value;
        if(email.length > 0){
          if(!isEmailValid(email)){
            setFormError((prevFormError) => ({ ...prevFormError, ['email']: 'E-mail inválido.' }));
          }else{
            checkEmailValidityUsingAPI(email);
          }
        }
      }, 500);

      setEmailTimer(newTimer);
  }

  const validateInput = (e) => {
    let isInputValid = true;

    if(formData.name.length == 0){
      setFormError((prevFormError) => ({ ...prevFormError, ['name']: 'Nome não pode ser vazio.' }));
      isInputValid = false;
    }

    if(formData.email.length == 0){
      setFormError((prevFormError) => ({ ...prevFormError, ['email']: 'E-mail não pode ser vazio.' }));
      isInputValid = false;
    }else if(!isEmailValid(formData.email)){
      setFormError((prevFormError) => ({ ...prevFormError, ['email']: 'E-mail inválido.' }));
      isInputValid = false;
    }else if(emailStatus != "VALIDO"){
      setFormError((prevFormError) => ({ ...prevFormError, ['email']: 'E-mail inválido.' }));
      isInputValid = false;
    }

    if(formData.cpf.length == 0){
      setFormError((prevFormError) => ({ ...prevFormError, ['cpf']: 'CPF não pode ser vazio.' }));
      isInputValid = false;
    }else if(!isCpfValid(formData.cpf)){
      setFormError((prevFormError) => ({ ...prevFormError, ['cpf']: 'CPF inválido.' }));
      isInputValid = false;
    }else if(cpfStatus != "REGULAR"){
      setFormError((prevFormError) => ({ ...prevFormError, ['cpf']: 'CPF irregular.' }));
      isInputValid = false;
    }

    return isInputValid;
  };

  const isCpfValid = (cpf_string) => {
    const cpf = onlyNumbers(cpf_string);
    if ((cpf.length !== 11) || (/^(\d)\1+$/.test(cpf))){
      return false;
    }

    let soma = 0;
    for (let i = 0; i < 9; i++) {
        soma += parseInt(cpf.charAt(i)) * (10 - i);
    }
    let first_digit = (soma % 11);
    if (first_digit >= 2) 
      first_digit = 11 - first_digit;
    else
      first_digit = 0;

    soma = 0;
    for (let i = 0; i < 10; i++) {
        soma += parseInt(cpf.charAt(i)) * (11 - i);
    }
    let second_digit = (soma % 11);
    if (second_digit >= 2) 
      second_digit = 11 - second_digit;
    else
      second_digit = 0;

    return ((parseInt(cpf.charAt(9)) == first_digit) && (parseInt(cpf.charAt(10)) == second_digit)) 
  };

  const isEmailValid = (email_addr) => {
    const regEx = /[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,8}(.[a-z{2,8}])?/g;
    if (regEx.test(email_addr)) {
      return true;
    } else if (!regEx.test(email_addr) && email_addr !== "") {
      return false;
    } 
  };

  const handleSubmit = async (e) => {
    setLoadingSave(true);
    if (validateInput()) {
      const cpf = onlyNumbers(formData.cpf);
      let payload;
      await generateQuestionnaire(cpf).then(response => {
        try{
          payload = response.data;
          setOpenQuestionnaire(true);
          setQuestionnaireData(payload);
        }catch(e){
          showNotification("Erro ao acessar a API de geração de questionários", "error");
        }finally{
          setLoadingSave(false);
        }});
    } else {
        setLoadingSave(false);
    }
  };

    return (
      <div className="create-form">
        <h2>Cadastro de Usuários</h2>
        <form autoComplete="off" onSubmit={(e) =>{e.preventDefault();}}>
          <label>CPF {(cpfStatus==="REGULAR")&&<span>✅</span>}</label>
          {loadingCpf && <CircularProgress size={25} style={{'color': '#0068ff'}}/>}
            <InputMask
            mask="999.999.999-99"
            type="text"
            required
            disabled = {(loadingCpf)? "disabled" : ""}
            name="cpf" value={formData.cpf} onChange={handleCPFChange}/>
          {formError.cpf && <p style={{color:"#ff003c", 'textAlign':'left'}}> {formError.cpf} </p>}
          <label>Nome</label>
            <input
            type="text"
            required
            name="name" value={formData.name} onChange={handleInputChange}/>
          {formError.name && <p style={{color:"#ff003c", 'textAlign':'left'}}> {formError.name} </p>}
          <label>E-mail {(emailStatus==="VALIDO")&&<span>✅</span>}</label>
          {loadingEmail && <CircularProgress size={25} style={{'color': '#0068ff'}}/>}
            <input
            type="text"
            required
            disabled = {(loadingEmail)? "disabled" : ""}
            name="email" value={formData.email} onChange={handleEmailChange}/>
          {formError.email && <p style={{color:"#ff003c", 'textAlign':'left'}}> {formError.email} </p>}
          <ThemeProvider theme={customTheme}>
          { loadingSave && <CircularProgress color='big-blue'/> }
          { !loadingSave &&
            <Button 
            variant="contained" disabled={loadingCpf || loadingEmail}
            color="big-blue" onClick={handleSubmit}>Salvar</Button>
          }
          </ThemeProvider>
        </form>
        </div>
    );
}

export default SaveForm;