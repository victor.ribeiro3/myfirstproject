import { createTheme } from '@mui/material/styles';

let t = createTheme({});
  
const theme = createTheme(t, {
    palette: {
        "big-blue": t.palette.augmentColor({
        color: {
            main: '#0068ff',
        },
        name: 'big-blue',
        }),
        "loyal-red": t.palette.augmentColor({
        color: {
            main: '#ff003c',
        },
        name: 'loyal-red',
        }),
    },
});

export default theme;