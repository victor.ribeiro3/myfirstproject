import { Snackbar, Alert } from "@mui/material";
import React, { useState } from "react";

const Notification = ({  open, handleClose, message, type }) => {

  return (
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}
                anchorOrigin={{ vertical:"bottom", horizontal:'center' }}>
          <Alert onClose={handleClose} severity={type} variant="filled">
              {message}
          </Alert>
      </Snackbar>
  )
};

export default Notification;