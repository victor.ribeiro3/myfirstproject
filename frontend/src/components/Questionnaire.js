import React from 'react';
import "./Questionnaire.css";
import { Button, CircularProgress } from '@mui/material';
import { ThemeProvider } from '@mui/material/styles';
import theme from './Theme';
import { useState } from 'react';
import { sendQuestionnaireAnswers } from '../api/BigID';
import { saveUser } from '../api/User';
import Notification from './Notification';

function Questionnaire({openQuestionnaire, questionnaireData, setOpenQuestionnaire, formData, showNotification}) {

  const [selectedAnswers, setSelectedAnswers] = useState({});
  const [loadingSendAnswer, setLoadingSendAnswer] = useState(false);

  const handleAnswerChange = (questionIndex, answer) => {
    setSelectedAnswers({
      ...selectedAnswers,
      [questionIndex]: answer,
    });
  };

  const onlyNumbers = (s) => {
    return s.replace(/[^0-9]/g, '');
  };

  const sendAnswers = async (e) => {
    setLoadingSendAnswer(true);
    const ticketId = questionnaireData.TicketId;
    const answersList = Object.values(selectedAnswers);
    let payload;
    await sendQuestionnaireAnswers(ticketId, answersList).then(response => {
      try{
        payload = response.data;
      }catch(e){
        showNotification('Erro ao acessar a API de envio de resposta de questionário', 'error');
        setLoadingSendAnswer(false);
        return;
      }finally{
      }});


    if (payload){
      if (payload?.ResultMessage === 'Validated'){
        setTimeout(() => {
            let userData = formData;
            userData.cpf = onlyNumbers(userData.cpf);
            saveUser(userData)
                .then((response) => {
                    if (response.status === 201) {
                        showNotification("Usuário salvo no banco de dados!", 'success');
                    } else {
                        showNotification("Erro ao salvar o usuário no banco de dados!", 'error');
                    }
                })
                .catch(() => showNotification("Erro ao acessar a API.", 'error'))
                .finally(() => {closeQuestionnaire()});
        }, 200);
      }else{
        // alert("Respostas do questionário não validadas!");
        showNotification("Respostas do questionário não validadas!", 'error');
        closeQuestionnaire();
      }
    }
  };

  const closeQuestionnaire = (e) => {
    setSelectedAnswers({});
    setOpenQuestionnaire(false);
    setLoadingSendAnswer(false);
  };

  const isSubmitButtonEnabled = Object.keys(selectedAnswers).length === questionnaireData?.Questions.length;

  const renderQuestions = () => {
    return questionnaireData.Questions.map((question, index) => (
      <div key={index}>
        <h4>{question.Question}</h4>
        <form>
          {question.Answers.map((answer, answerIndex) => (
            <div key={answerIndex}>
              <label>
                <input
                  type="radio"
                  name={`question-${index}`}
                  value={answer}
                  checked={selectedAnswers[index] === answer}
                  onChange={() => handleAnswerChange(index, answer)}
                />
                {answer}
              </label>
            </div>
          ))}
        </form>
      </div>
    ));
  };

  if(openQuestionnaire){
    return (
      <div className="modal">
        <div className="overlay"></div>
        <div className="modal-content">
          <h3>Questionário de Validação de Identidade</h3>
          <div className="questions-form">
            {renderQuestions()}
          </div>
          <div className="action-buttons">
            <ThemeProvider theme={theme}>
              <Button variant="contained" color="loyal-red" style={{"width":'130px'}} onClick={() => closeQuestionnaire()}>Fechar</Button>
              { !loadingSendAnswer &&
                <Button variant="contained" color="big-blue" style={{"width":'130px'}}
                        disabled={!isSubmitButtonEnabled} onClick={sendAnswers}>Enviar</Button>
              }
              { loadingSendAnswer &&
                <CircularProgress/>
              }
            </ThemeProvider>
          </div>
        </div>
      </div>
    )
  }
};

export default Questionnaire;