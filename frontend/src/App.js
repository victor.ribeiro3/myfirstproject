import './App.css';
import './index.css'
import SaveForm from './components/SaveForm';
import Questionnaire from './components/Questionnaire';
import Notification from './components/Notification';
import { useState } from 'react';

function App() {
  
  const [openQuestionnaire, setOpenQuestionnaire] = useState(false);
  const [questionnaireData, setQuestionnaireData] = useState();
  const [formData, setFormData] = useState({cpf: "", name: "",email: ""});
  const [notificationState, setNotificationState] = useState({
    open: false,
    message: "",
    type: "error",
  });

  const showNotification = (message, type) => {
    setNotificationState({
      open: true,
      message: message,
      type: type,
    });
  };

  const handleNotificationClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setNotificationState((prevState) => ({ ...prevState, open: false }));
  };

  return (
    <div className="App">
      <header className="App-header">
        <h1>
          My First App
        </h1>
      </header>
    <SaveForm 
      setOpenQuestionnaire={setOpenQuestionnaire} setQuestionnaireData={setQuestionnaireData}
      formData={formData} setFormData={setFormData} showNotification={showNotification}
      />
    <Questionnaire 
      setOpenQuestionnaire={setOpenQuestionnaire} openQuestionnaire={openQuestionnaire} 
      questionnaireData={questionnaireData}
      formData={formData}
      showNotification={showNotification}/>
    <Notification
          open={notificationState.open}
          handleClose={handleNotificationClose}
          message={notificationState.message}
          type={notificationState.type}
        />
    </div>
  );
}

export default App;
