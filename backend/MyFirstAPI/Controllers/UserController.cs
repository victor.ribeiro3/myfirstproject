﻿using Microsoft.AspNetCore.Mvc;
using MyFirstAPI.Models;
using MyFirstAPI.Services;

namespace MyFirstAPI.Controllers;

[ApiController]
[Route("api/[controller]")]
public class UserController: Controller
{
    private readonly MongoDBService _mongDBService;

    public UserController(MongoDBService mongDBService)
    {
        _mongDBService = mongDBService;
    }

    [HttpGet]
    public async Task<IEnumerable<User>> GetAllUser()
    {
        return await _mongDBService.GetAllUsersAsync();
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetUserById(string id)
    {
        var user = await _mongDBService.GetOneUserAsync(id);
        if (user == null)
        {
            return NotFound();
        }
        return Ok(user);
    }

    [HttpPost("save")]
    public async Task<IActionResult> AddUser([FromBody] User user)
    {
        await _mongDBService.CreateUserAsync(user);
        return CreatedAtAction(nameof(GetAllUser), new {id = user.Id}, user);
    }
}
