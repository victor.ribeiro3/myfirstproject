﻿using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using MyFirstAPI.Models;

namespace MyFirstAPI.Services;

public class MongoDBService
{
    private readonly IMongoCollection<User> _usersCollection;

    public MongoDBService(IOptions<MongoDBSettings> settings)
    {
        MongoClient client = new MongoClient(settings.Value.ConnectionURI);
        IMongoDatabase database = client.GetDatabase(settings.Value.DatabaseName);
        _usersCollection = database.GetCollection<User>(settings.Value.CollectionName);

    }

    public async Task CreateUserAsync(User user)
    {
        await _usersCollection.InsertOneAsync(user);
        return;
    }

    public async Task<List<User>> GetAllUsersAsync()
    {
        return await _usersCollection.Find(new BsonDocument()).ToListAsync();
    }

    public async Task<User> GetOneUserAsync(string id)
    {
        FilterDefinition<User> filter = Builders<User>.Filter.Eq("Id", id);
        return await _usersCollection.Find(filter).FirstOrDefaultAsync();
    }
    
}
